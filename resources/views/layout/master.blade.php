<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<!--<![endif]-->
<head>
@include('partials.head')
</head>
<body>
	<!-- begin #page-loader -->
	<div id="page-loader" class="fade"><span class="spinner"></span></div>
	<!-- end #page-loader -->
	<!-- begin #page-container -->
	<div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
		<!-- begin #header -->
		<div id="header" class="header navbar navbar-default navbar-fixed-top">
		@include('partials.headernavbar')
		</div>
		<!-- end #header -->
		<!-- begin #sidebar -->
		<div id="sidebar" class="sidebar">
		@include('partials.sidebar')
		</div>
		<div class="sidebar-bg"></div>
		<!-- end #sidebar -->
		<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			@if(count($data['breadcrumbs']) > 0)
				<ol class="breadcrumb pull-right">
					@foreach($data['breadcrumbs'] as $key => $breadcrumb)
					<li><a @if($breadcrumb != null) href="{{ $breadcrumb }}" @else href="" @endif>{{ $key }}</a></li>							
					@endforeach
				</ol>
			@endif
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">{{ __('global.app.title.full') }} </h1>
			<!-- end page-header -->
			@yield('content')
		</div>
		<!-- begin scroll to top btn -->
		<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
		<!-- end scroll to top btn -->
	</div>
	<!-- end page container -->
	@include('partials.scripts')
</body>
</html>