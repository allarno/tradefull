@extends('layout.master')
@section('content')		
<!-- begin row -->
<div class="row">
	<!-- begin col-3 -->
	<div class="col-md-3 col-sm-6">
		<div class="widget widget-stats bg-green">
			<div class="stats-icon"><i class="fa fa-child"></i></div>
			<div class="stats-info">
				<h4>{{ __('global.user.students.total') }}</h4>
				<p>{{ \App\StudentProfile::count() }}</p>	
			</div>
			<div class="stats-link">
				<a href="javascript:;">{{ __('global.actions.view_details') }} <i class="fa fa-arrow-circle-o-right"></i></a>
			</div>
		</div>
	</div>
	<!-- end col-3 -->
	<!-- begin col-3 -->
	<div class="col-md-3 col-sm-6">
		<div class="widget widget-stats bg-blue">
			<div class="stats-icon"><i class="fa fa-user-plus"></i></div>
			<div class="stats-info">
				<h4>{{ __('global.user.guardians.total') }}</h4>
				<p>{{ \App\GuardianProfile::count() }}</p>	
			</div>
			<div class="stats-link">
				<a href="javascript:;">{{ __('global.actions.view_details') }} <i class="fa fa-arrow-circle-o-right"></i></a>
			</div>
		</div>
	</div>
	<!-- end col-3 -->
	<!-- begin col-3 -->
	<div class="col-md-3 col-sm-6">
		<div class="widget widget-stats bg-purple">
			<div class="stats-icon"><i class="fa fa-users"></i></div>
			<div class="stats-info">
				<h4>{{ trans_choice('global.user.students.new', 2) }}</h4>
				<p></p>	
			</div>
			<div class="stats-link">
				<a href="javascript:;">{{ __('global.actions.view_details') }} <i class="fa fa-arrow-circle-o-right"></i></a>
			</div>
		</div>
	</div>
	<!-- end col-3 -->
	<!-- begin col-3 -->
	<div class="col-md-3 col-sm-6">
		<div class="widget widget-stats bg-red">
			<div class="stats-icon"><i class="fa fa-clock-o"></i></div>
			<div class="stats-info">
				<h4></h4>
				<p></p>	
			</div>
			<div class="stats-link">
				<a href="javascript:;">{{ __('global.actions.view_details') }} <i class="fa fa-arrow-circle-o-right"></i></a>
			</div>
		</div>
	</div>
	<!-- end col-3 -->
</div>
<!-- end row -->
<div class="row">
	<!-- begin col-12 -->
	<div class="col-md-12">
		<div class="table-responsive">
			@include('partials.students.all')
		</div>
	</div>
	<!-- end col-12 -->
</div>
<!-- end row -->
@stop