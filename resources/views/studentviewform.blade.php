@extends('layout.master')
@section('content')
<!-- begin row -->
<div class='row'>
    <!-- begin col-8 -->
    <div class="col-md-8">
        @if(Session::has('success'))
            <div class="alert alert-success messages">
                {{ __('global.messages.success.new') }}
                @php
                    Session::forget('success');
                @endphp
            </div>
        @endif
        <div class="row">
            <ul class="nav nav-pills">
                <li class="active"><a href="#nav-pills-student" data-toggle="tab">{{ trans_choice('global.user.students.details', 1) }}</a></li>
                <li><a href="#nav-pills-guardian" data-toggle="tab">Guardian</a></li>
                <li><a href="#nav-pills-guardians" data-toggle="tab">Guardians</a></li>
            </ul>
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="nav-pills-student">
                        <form class="form-horizontal" method="POST" action="{{ route('student.update') }}">
                            @csrf
                            @method('PUT')
                            <div class="form-group @error('first_name') is-invalid has-error has-feedback @enderror">
                                <label class="col-md-3 control-label">{{ __('global.user.first_name') }}</label>
                                <div class="col-md-9">
                                    <input type="text" name="first_name" value="{{ old('first_name', $data['student']['first_name']) }}" class="form-control" placeholder="{{ __('global.user.first_name') }}" />
                                    @error('first_name')
                                        <span class="text-danger">{{ $message }}</span>
                                        <span class="fa fa-times form-control-feedback"></span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group @error('last_name') is-invalid has-error has-feedback @enderror">
                                <label class="col-md-3 control-label">{{ __('global.user.last_name') }}</label>
                                <div class="col-md-9">
                                    <input type="text" name='last_name' value="{{ old('last_name', $data['student']['last_name']) }}" class="form-control" placeholder="{{ __('global.user.last_name') }}" />
                                    @error('last_name')
                                        <span class="text-danger">{{ $message }}</span>
                                        <span class="fa fa-times form-control-feedback"></span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ __('global.user.other_name') }}</label>
                                <div class="col-md-9">
                                    <input type="text" name='other_name' value="{{ old('other_name', $data['student']['other_name']) }}" class="form-control" placeholder="{{ __('global.user.other_name') }}" />
                                    @error('other_name')
                                        <span class="text-danger">{{ $message }}</span>
                                        <span class="fa fa-times form-control-feedback"></span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group @error('admission_number') is-invalid has-error has-feedback @enderror">
                                <label class="col-md-3 control-label">{{ __('global.user.students.admission_number') }}</label>
                                <div class="col-md-9">
                                    <input type="text" name='admission_number' readonly="readonly" value="{{ old('admission_number', $data['student']['admission_number']) }}" class="form-control" placeholder="{{ __('global.user.student.admission_number') }}" />
                                    @error('admission_number'))
                                        <span class="text-danger">{{ $message }}</span>
                                        <span class="fa fa-times form-control-feedback"></span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group @error('sex') is-invalid has-error has-feedback @enderror">
                                <label class="col-md-3 control-label">{{ __('global.sex') }}</label>
                                <div class="col-md-9">
                                    <select class="form-control" name="sex">
                                        <option value="">{{ __('global.messages.select_sex') }}</option>
                                        @forelse($data['sex'] as $sex)
                                        <option value="{{ Illuminate\Support\Str::lower($sex) }}">{{ $sex }}</option>
                                        @empty
                                        <option value="">{{ __('global.messages.nothing_to_display') }}</option>
                                        @endforelse
                                    </select>
                                    @error('sex'))
                                        <span class="text-danger">{{ $message }}</span> 
                                        <span class="fa fa-times form-control-feedback"></span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group @error('date_of_birth') is-invalid has-error has-feedback @enderror">
                                <label class="col-md-3 control-label">{{ __('global.user.date_of_birth') }}</label>
                                <div class="col-md-9">
                                    <input type="text" name='date_of_birth' value="{{ old('date_of_birth', \App\UserProfile::getDateOfBirthAttribute($data['student']['date_of_birth'])) }}" class="form-control" id="datepicker-autoClose" placeholder="{{ __('global.user.date_of_birth') }}" />
                                    @error('date_of_birth'))
                                        <span class="text-danger">{{ $message }}</span>
                                        <span class="fa fa-times form-control-feedback"></span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group @error('active') is-invalid has-error has-feedback @enderror">
                                <label class="col-md-3 control-label">{{ __('global.status.status') }}</label>
                                <div class="col-md-9">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="active" value="1" checked />
                                            {{ __('global.status.attributes.active') }}
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="active" value="0" />
                                            {{ __('global.status.attributes.inactive') }}
                                        </label>
                                    </div>
                                    @error('active'))
                                        <span class="text-danger">{{ $message }}</span>
                                        <span class="fa fa-times form-control-feedback"></span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-9">
                                    <button type="submit" class="btn btn-sm btn-success">{{ __('global.actions.update') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane fade" id="nav-pills-guardian">
                        <div class="alert alert-warning">
                            {{ __('global.enter_guardian_details') }}
                        </div>
                        <form class="form-horizontal" method="POST" action="{{ route('student.guardian.create')}}">
                            @csrf
                            <div class="form-group @error('first_name') is-invalid has-error has-feedback @enderror">
                                <label class="col-md-3 control-label">{{ __('global.user.first_name') }}</label>
                                <div class="col-md-9">
                                    <input type="text" name="first_name" value="{{ old('first_name') }}" class="form-control" placeholder="{{ __('global.user.first_name') }}" />
                                    @error('first_name')
                                        <span class="text-danger">{{ $message }}</span>
                                        <span class="fa fa-times form-control-feedback"></span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group @error('last_name') is-invalid has-error has-feedback @enderror">
                                <label class="col-md-3 control-label">{{ __('global.user.last_name') }}</label>
                                <div class="col-md-9">
                                    <input type="text" name='last_name' value="{{ old('last_name') }}" class="form-control" placeholder="{{ __('global.user.last_name') }}" />
                                    <input type='hidden' name='student_id' value="{{ $data['student']['id'] }}"></input>
                                    @error('last_name')
                                        <span class="text-danger">{{ $message }}</span>
                                        <span class="fa fa-times form-control-feedback"></span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">{{ __('global.user.other_name') }}</label>
                                <div class="col-md-9">
                                    <input type="text" name='other_name' value="{{ old('other_name') }}" class="form-control" placeholder="{{ __('global.user.other_name') }}" />
                                    @error('other_name')
                                        <span class="text-danger">{{ $message }}</span>
                                        <span class="fa fa-times form-control-feedback"></span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group @error('relationship') is-invalid has-error has-feedback @enderror">
                                <label class="col-md-3 control-label">{{ __('global.relationships.relationship') }}</label>
                                <div class="col-md-9">
                                    <input type="text" name='relationship' value="{{ old('relationship') }}" class="form-control" placeholder="{{ __('global.relationships.relationship') }}" />
                                    @error('relationship')
                                        <span class="text-danger">{{ $message }}</span>
                                        <span class="fa fa-times form-control-feedback"></span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group @error('sex') is-invalid has-error has-feedback @enderror">
                                <label class="col-md-3 control-label">{{ __('global.sex') }}</label>
                                <div class="col-md-9">
                                    <select class="form-control" name="sex">
                                        @foreach($data['sex'] as $sex)
                                        <option value="{{ Illuminate\Support\Str::lower($sex) }}">{{ $sex }}</option>
                                        @endforeach
                                    </select>
                                    @error('sex'))
                                        <span class="text-danger">{{ $message }}</span> 
                                        <span class="fa fa-times form-control-feedback"></span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group @error('active') is-invalid has-error has-feedback @enderror">
                                <label class="col-md-3 control-label">{{ __('global.status.status') }}</label>
                                <div class="col-md-9">
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="active" value="1" checked />
                                            {{ __('global.status.attributes.active') }}
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="active" value="0" />
                                            {{ __('global.status.attributes.inactive') }}
                                        </label>
                                    </div>
                                    @error('active')
                                        <span class="text-danger">{{ $message }}</span>
                                        <span class="fa fa-times form-control-feedback"></span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-9">
                                    <button type="submit" class="btn btn-sm btn-success">{{ __('global.actions.save') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane fade" id="nav-pills-guardians">
                        @include('partials.guardians.all')
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end col-8 -->
</div>
<!-- end row -->
@stop