<!-- ================== BEGIN BASE JS ================== -->
<script src="{{ asset('plugins/jquery/jquery-1.9.1.min.js') }}"></script>
<script src="{{ asset('plugins/jquery/jquery-migrate-1.1.0.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-ui/ui/minified/jquery-ui.min.js') }}"></script>
<script src="{{ asset('plugins/bootstrap/js/bootstrap.min.js') }}"></script>
<!--[if lt IE 9]>
	<script src="assets/crossbrowserjs/html5shiv.js"></script>
	<script src="assets/crossbrowserjs/respond.min.js"></script>
	<script src="assets/crossbrowserjs/excanvas.min.js"></script>
<![endif]-->
<script src="{{ asset('plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-cookie/jquery.cookie.js') }}"></script>
<!-- ================== END BASE JS ================== -->

@if(\Route::currentRouteName() == 'dashboard')	
<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="{{ asset('plugins/gritter/js/jquery.gritter.js') }}"></script>
<script src="{{ asset('plugins/flot/jquery.flot.min.js') }}"></script>
<script src="{{ asset('plugins/flot/jquery.flot.time.min.js') }}"></script>
<script src="{{ asset('plugins/flot/jquery.flot.resize.min.js') }}"></script>
<script src="{{ asset('plugins/flot/jquery.flot.pie.min.js') }}"></script>
<script src="{{ asset('plugins/sparkline/jquery.sparkline.js') }}"></script>
<script src="{{ asset('plugins/jquery-jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('js/dashboard.min.js') }}"></script>
<!-- ================== END PAGE LEVEL JS ================== -->
@endif

@if(\Route::currentRouteName() == 'student.new' || \Route::currentRouteName() == 'student.view')
<script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('plugins/ionRangeSlider/js/ion-rangeSlider/ion.rangeSlider.min.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js') }}"></script>
<script src="{{ asset('plugins/masked-input/masked-input.min.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}"></script>
<script src="{{ asset('plugins/password-indicator/js/password-indicator.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-combobox/js/bootstrap-combobox.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-select/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-tagsinput/bootstrap-tagsinput-typeahead.js') }}"></script>
<script src="{{ asset('plugins/jquery-tag-it/js/tag-it.min.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-daterangepicker/moment.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('plugins/select2/dist/js/select2.min.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-eonasdan-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('js/form-plugins.demo.min.js') }}"></script>
@endif
<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="{{ asset('js/apps.min.js') }}"></script>
<!-- ================== END PAGE LEVEL JS ================== -->
<script type="text/javascript">
	$.ajaxSetup({
	    headers: {
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
	});
</script>
<script>
	$(document).ready(function() {
		App.init();
		@if(\Route::currentRouteName() == 'student.new' || \Route::currentRouteName() == 'student.view')
		FormPlugins.init();
		@endif
	});
</script>

<script>
  $(document).ready(function(){
      $('button.destroy_student').on('click', function(){
        	$.ajax({
        		url: "{{ route('student.destroy') }}",
        		type: "post",
        		cache: false,
        		data:{'_method' : 'DELETE', id: $(this).data("id")},
        		success: function(results){
        			results = JSON.parse(results);
        			if(results.statusCode === 200){
        				$(".panel-body").html('<div class="alert alert-success">Record deleted</div>');
        			}
        		}
        	});
      });
  });
</script>

<script type="text/javascript" src="{{ asset('js/custom.js') }}"></script>