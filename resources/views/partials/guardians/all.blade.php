<table class="table">
	<thead>
		<tr>
			<th>#</th>
			<th>{{ __('global.user.names') }}</th>			
			<th>{{ trans_choice('global.user.students.student', 2) }}</th>
			<th>{{ __('global.user.sex') }}</th>
			<th>{{ __('global.relationships.relationship') }}</th>
		</tr>
	</thead>
	<tbody>
		@forelse($data['guardians'] as $key => $guardian)
		<tr>
			<td>{{ ++$key }}</td>
			<td>{{ $guardian->first_name }} {{ $guardian->last_name }}</td>
			<td>
				<span class="badge badge-primary">
				{{ App\GuardianProfile::find($guardian->student_id)->student->first_name }} 
				{{ App\GuardianProfile::find($guardian->student_id)->student->last_name }}
				</span>
			</td>
			<td>{{ $guardian->sex }}</td>
			<td>{{ $guardian->relationship }}</td>
		</tr>
		@empty		
		<tr>
			<td colspan="4">{{ __('global.messages.no_data') }}</td>
		</tr>
		@endforelse		
	</tbody>
</table>