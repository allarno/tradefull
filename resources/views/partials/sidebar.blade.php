<!-- begin sidebar scrollbar -->
<div data-scrollbar="true" data-height="100%">
	<!-- begin sidebar nav -->
	<ul class="nav">
		<li class="{{ request()->routeIs('dashboard') ? 'active' : '' }}" }}><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> <span>{{ __('global.dashboard') }}</span></a></li>
		<li class="has-sub {{ request()->routeIs('student*') ? 'active' : '' }}">
		    <a href="javascript:;">
			    <b class="caret pull-right"></b>
		        <i class="fa fa-child"></i>
		        <span>{{ trans_choice('global.user.students.student', 2) }}</span>
		    </a>
			<ul class="sub-menu">
				<li class="{{ request()->routeIs('students') ? 'active' : '' }}"><a href="{{ route('students') }}">{{ trans_choice('global.user.students.student', 2) }}</a></li>
				<li class="{{ request()->routeIs('student.new') ? 'active' : '' }}"><a href="{{ route('student.new') }}">{{ trans_choice('global.user.students.new', 1) }}</a></li>
			</ul>
		</li>
		<li class="{{ request()->routeIs('guardians*') ? 'active' : '' }}"><a href="{{ route('guardians') }}"><i class="fa fa-home"></i> <span>{{ trans_choice('global.user.guardians.guardian', 1) }}</span></a></li>
	</ul>
	<!-- end sidebar nav -->
</div>
<!-- end sidebar scrollbar -->