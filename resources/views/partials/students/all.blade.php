<table class="table">
	<thead>
		<tr>
			<th>#</th>
			<th>{{ __('global.user.names') }}</th>
			<th>{{ __('global.user.students.admission_number') }}</th>
			<th>{{ __('global.user.sex') }}</th>
			<th>{{ __('global.user.date_of_birth') }}</th>
			<th>{{ trans_choice('global.actions.action', 2)}}</th>
		</tr>
	</thead>
	<tbody>
		@forelse($data['students'] as $key => $student)
		<tr>
			<td>{{ ++$key }}</td>
			<td>{{ $student->first_name }} {{ $student->last_name }}</td>
			<td>{{ $student->admission_number }}</td>
			<td>{{ $student->sex }}</td>
			<td>{{ $student->date_of_birth }}</td>
			<td>
				<div class="col-lg-3 col-md-3 col-xs-6">
				<a class="btn btn-success btn-xs" href="{{ route('student.view', $student->id) }}">{{ __('global.actions.view') }}</a>
				</div>
				<div class="col-lg-3 col-md-3 col-xs-6">
					<button class="btn btn-xs btn-danger destroy_student" data-id="{{ $student->id }}">{{ __('global.actions.delete') }}</button>
				</div>
			</td>
		</tr>
		@empty
		<tr>
			<td colspan="6">{{ __('global.messages.no_data') }}</td>
		</tr>
		@endforelse			
	</tbody>
</table>