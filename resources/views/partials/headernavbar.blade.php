<!-- begin container-fluid -->
<div class="container-fluid">
	<!-- begin mobile sidebar expand / collapse button -->
	<div class="navbar-header">
		<a href="{{ route('dashboard') }}" class="navbar-brand"><span class="navbar-logo"></span> {{ __('global.app.title.short') }}</a>
		<button type="button" class="navbar-toggle" data-click="sidebar-toggled">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
	</div>
	<!-- end mobile sidebar expand / collapse button -->
	
	<!-- begin header navigation right -->
	<ul class="nav navbar-nav navbar-right">
		<li>
			<form class="navbar-form full-width">
				<div class="form-group">
					<input type="text" class="form-control" placeholder="{{ __('global.app.search.enter_key_word') }}" />
					<button type="submit" class="btn btn-search"><i class="fa fa-search"></i></button>
				</div>
			</form>
		</li>
		<li class="dropdown navbar-user">
			<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
				<span class="hidden-xs">{{ __('global.app.language.pick') }}</span> <b class="caret"></b>
			</a>
			<ul class="dropdown-menu animated fadeInLeft">
				<li class="arrow"></li>
				<li><a href="{{ route('language', 'en') }}"><img class='country-flag' src="{{ asset('img/country-flags/usa.png') }}" alt="{{ __('global.app.language.english-us') }}" /> {{ __('global.app.language.english-us') }}</a></li>
				<li class="divider"></li>
				<li><a href="{{ route('language', 'swa') }}"><img class='country-flag' src="{{ asset('img/country-flags/kenya.png') }}" alt="{{ __('global.app.language.kiswahili-ken') }}" /> {{ __('global.app.language.kiswahili-ken') }}</a></li>
			</ul>
		</li>
		<li class="dropdown">
			<ul class="dropdown-menu media-list pull-right animated fadeInDown">
                <li class="dropdown-header">Notifications (5)</li>
                <li class="">
                    <a href="javascript:;">
                        <div class="media-left"><i class="fa fa-bug media-object bg-red"></i></div>
                        <div class="media-body">
                            
                        </div>
                    </a>
                </li>
            </ul>
        </li>
	</ul>
	<!-- end header navigation right -->
</div>
<!-- end container-fluid -->