@extends('layout.master')
@section('content')
<!-- begin row -->
<div class="row">
	<div class="col-md-12">
	    <!-- begin panel -->
	    <div class="panel panel-inverse" data-sortable-id="table-basic-7">
	        <div class="panel-heading">
	            <h4 class="panel-title">{{ trans_choice('global.user.guardians.guardian', 2) }}</h4>
	        </div>
	        <div class="panel-body">
				<div class="table-responsive">
					@include('partials.guardians.all')
				</div>
			</div>
		</div>
	    <!-- end panel -->
	</div>
	<!-- end col-12 -->
</div>
<!-- end row -->
@stop