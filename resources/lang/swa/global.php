<?php
	return [	
		'app' => [
			'title' => [
				'short' => 'SIS',
				'full' =>'Usimamizi Wa Mfumo Wa Wanafunzi'
					],
			'language' => [
				'pick' => 'Badilisha Lugha',
				'english-us' => 'Kingereza - US',
				'english-uk' => 'Kingereza - UK',
				'englisha-ua' => 'Kingereza - UA',
				'english-can' => 'Kingereza - CAN',
				'kiswahili-ken' => 'Kiswahili - Kenya'
				],
			'welcome_user' => 'Karibu, :Name',
			'login' => 'Ingia',
			'search' => [
				'verb' => 'Tafuta',
				'keyword' => 'Neno kuu',
				'enter_key_word' => 'Bonyeza neno kuu'
			],
			'home' => 'Nyumbani'
			],	
		'dashboard' => 'Dashibodi',
		'sex' => 'Jinsia',
		'user' => [
			'name' => 'Jina',
			'names' => 'Majina',
			'first_name' => 'Jina la kwanza',
			'last_name' => 'Jina la mwisho',
			'other_name' => 'Jina lingine',
			'date_of_birth' => 'Tarehe ya kuzaliwa',
			'sex' => 'Jinsia',
			'students' => [
				'student' => 'Mwanafunzi|Wanafunzi',
				'admission_number' => 'Nambari ya usajili',
				'total' => 'Jumla ya wanafunzi',
				'new' => 'Mwanafunzi mgeni|Wanafunzi wageni',
				'details' => 'Maelezo ya mwanafunzi'
				],
			'guardians' => [
				'guardian' => 'Mlezi|Walezi',
				'total' => 'Jumla ya walezi'
				],
			],
		'error' => [
			'need_sign_in' => 'Unahitaji kuwa umeingia kwenye mfumo huu ili kuitazama kuarasa huu.',
			'incorrect_credentials' => 'Maelezo uliyopeana sio sahihi',
			],
		'status' => [
			'status' => 'Hali',
			'attributes' => [
				'active' => 'Active',
				'inactive' => 'Inactive'
				]
			],
		'actions' => [
			'action' => 'Hatua|Hatua',
			'new' => 'Mpya',
			'create' => 'Jenga',
			'edit' => 'Hariri',
			'update' => 'Sahihisha',
			'delete' => 'Futa',
			'save' => 'Hifadhi',
			'view' => 'Tazama',
			'view_detail' => 'Tazama zaidi',
			'view_details' => 'Tazama zaidi',
			'message' => [
				'create' => ':name imejengwa',
				'edit' => ':name imehaririwa',
				'update' => ':name imesahihishwa',
				'delete' => ':name imefutwa',
				]
			],
		'messages' => [
			'no_data' => 'Hamna data',
			'success' => [
				'new' => 'Rekodi mpya imehifadhiwa kwa mafanikio',
				'updated' => 'Rekodi imesahihishwa kwa mafanikio'
				],
			'fail' => [
				'new' => 'Hitilafu limetokea wakati wa kuhifadhi rekodi',
				'updated' => 'Hitilafu limetokea wakati wa kuhifadhi rekodi'
				],
			'select_sex' => 'Chagua jinsia',
			'nothing_to_display' => 'Hamna cha kuonyesha',
		],
		'relationships' => [
			'relationship' => 'Uhusiano',
			'father' => 'Baba',
			'mother' => 'Mama',
			'uncle' => 'Mjomba',
			'aunt' => 'Shangazi',
			'grandfather' => 'Babu',
			'grandmother' => 'Nyanya',
			'godfather' => 'Baba wa ubatizo',
			'godmother' => 'Mama wa ubatizo',
			'cousin' => 'Binamu'
		],	
		'enter_guardian_details' => 'Jibu maelezo ya mlezi',
		'view_student' => 'Tazama mwanafunzi|Tazama wanafunzi'
	]
?>