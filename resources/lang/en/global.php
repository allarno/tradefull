<?php
	return [	
		'app' => [
			'title' => [
				'full' => 'Student Information System',
				'short' => 'SIS',
				],
			'language' => [
				'pick' => 'Change Language',
				'english-us' => 'English - US',
				'english-uk' => 'English - UK',
				'englisha-ua' => 'English - UA',
				'english-can' => 'English - CAN',
				'kiswahili-ken' => 'Kiswahili - Kenya'
				],			
			'welcome_user' => 'Welcome, :Name',
			'login' => 'Login',
			'search' => [
				'verb' => 'Search',
				'keyword' => 'Keyword',
				'enter_key_word' => 'Enter keyword'
			],
			'home' => 'Home'
			],
		'dashboard' => 'Dashboard',
		'sex' => 'Sex',
		'user' => [
			'name' => 'Name',
			'names' => 'Names',
			'first_name' => 'First name',
			'last_name' => 'Last name',
			'other_name' => 'Other name',
			'date_of_birth' => 'Date of birth',
			'sex' => 'Sex',
			'students' => [
					'student' => 'Student|Students',
					'new' => 'New student|New students',
					'admission_number' => 'Admission number',
					'total' => 'Total students',
					'details' => 'Student details'
					],
			'guardians' => [
				'guardian' => 'Guardian|Guardians',
				'total' => 'Total guardians'
				],
			],
		'error' => [
			'need_sign_in' => 'You need to sign in to view your dashboard',
			'incorrect_credentials' => 'Wrong username and password combo',
			],
		'status' => [
			'status' => 'Status',
			'attributes' => [
				'active' => 'Active',
				'inactive' => 'Inactive'
				]
			],
		'actions' => [
			'action' => 'Action|Actions',
			'new' => 'New',
			'create' => 'Create',
			'edit' => 'Edit',
			'update' => 'Update',
			'delete' => 'Delete',
			'save' => 'Save',
			'view' => 'View',
			'view_detail' => 'View Detail',
			'view_details' => 'View Details',
			'message' => [
				'create' => ':name has been created',
				'edit' => ':name has been edited',
				'update' => ':name has been updated',
				'delete' => ':name has been deleted',
				],
			],
		'messages' =>[
			'no_data' => 'No data',
			'success' => [
				'new' => 'New record has been successfully saved',
				'updated' => 'Record has been successfully updated'
				],
			'fail' => [
				'new' => 'An error occured while saving the new record',
				'updated' => 'An error occured while updating the new record'
				],
			'select_sex' => 'Select sex',
			'nothing_to_display' => 'Nothing to display',
		],
		'relationships' => [
			'relationship' => 'Relationship',
			'father' => 'Father',
			'mother' => 'Mother',
			'uncle' => 'Uncle',
			'aunt' => 'Aunt',
			'grandfather' => 'Grandfather',
			'grandmother' => 'Grandmother',
			'godfather' => 'Godfather',
			'godmother' => 'Godmother',
			'cousin' => 'Cousin'			
		],
		'enter_guardian_details' => 'Enter guardian details',
		'view_student' => 'View Student|View Students'
	]
?>