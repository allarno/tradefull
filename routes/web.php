<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\StudentProfileController;
use App\Http\Controllers\GuardianProfileController;
use App\Http\Controllers\LocalizationController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::group(['middleware' => 'localization'], function(){
	
// })

Route::get('setlocale/{lang}', [LocalizationController:: class, 'index'])->name('language');

Route::get('/',	[PagesController::class, 'dashboard'])->name('dashboard');
Route::get('dashboard',	[PagesController::class, 'dashboard'])->name('dashboard');
Route::get('students', [PagesController::class, 'students'])->name('students');


Route::get('guardians', [PagesController::class, 'guardians'])->name('guardians');
Route::group(['prefix' => 'student', 'as' => 'student.'], function(){
	Route::post('new', [StudentProfileController::class, 'createStudent'])->name('create');
	Route::get('new', [PagesController::class, 'studentForm'])->name('new');
	Route::get('{id}', [PagesController::class, 'studentViewForm'])->name('view');
	Route::put('update', [StudentProfileController::class, 'updateStudent'])->name('update');
	Route::delete('destroy', [StudentProfileController::class, 'deleteStudent'])->name('destroy');
	Route::group(['prefix' => 'guardian', 'as' => 'guardian.'], function(){
		Route::post('save-guardian', [GuardianProfileController::class, 'createGuardian'])->name('create');
		Route::put('save-guardian', [GuardianProfileController::class, 'updateGuardian'])->name('update');
	});
});