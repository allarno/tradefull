<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\StudentProfile;
use App\UserProfile;
use App\GuardianProfile;

use Lang;

class PagesController extends Controller
{
	public function __construct(){
        
	}
	public function dashboard(){
		$data = [
			'title' => __('global.dashboard'),
			'breadcrumbs' => [
				__('global.app.home') => null
				],
			'students' => StudentProfile::getStudents()
			];
		return view('dashboard', compact('data'));
	}

	public function studentForm(){
		$data = [
			'title' => trans_choice('global.user.student.new', 1),
			'breadcrumbs' =>[
				__('global.app.home') => '../dashboard', trans_choice('global.user.students.new', 1) => null
				],
			'admission_number' => $this->_generateAdmissionNumber(),
			'sex' => UserProfile::sex()
			];
		return view('studentform', compact('data'));
	}

	public function studentViewForm($id){
		$data = [
			'title' => trans_choice('global.view_student', 1),
			'breadcrumbs' =>[
				__('global.app.home') => '../dashboard', trans_choice('global.view_student', 1) => null
				],
			'sex' => UserProfile::sex(),
			'student' => StudentProfile::getStudent($id),
			'guardians' => StudentProfile::findOrFail($id)->guardians
			];
		return view('studentViewform', compact('data'));
	}

	public function students(){
		$data = [
			'title' => trans_choice('global.user.students.student', 2),
			'active' => 'active',
			'breadcrumbs' =>[
				__('global.app.home') => 'dashboard', trans_choice('global.user.students.student', 2) => null
				],
			'students' => StudentProfile::getStudents()
			];
		return view('students', compact('data'));
	}

	public function guardians(){
		$data = [
			'title' => trans_choice('global.user.guardians.guardian', 2),
			'active' => 'active',
			'breadcrumbs' =>[
				__('global.app.home') => 'dashboard', trans_choice('global.user.guardians.guardian', 2) => null
				],
			'guardians' => GuardianProfile::paginate(10)
			];
		return view('guardians', compact('data'));
	}

	private function _generateAdmissionNumber(){
		// Generate random numbers from 10000 to 99999
		$admission_number = mt_rand(10000, 99999);
		// Check if the number generated exists in the students table. 
		if($this->_admissionNumberExists($admission_number)){
			return _generateAdmissionNumber();
		}
		else{
			return $admission_number;
		}
	}

	private function _admissionNumberExists($admission_number){
		return StudentProfile::getAdmissionNumber($admission_number);
	}
}
