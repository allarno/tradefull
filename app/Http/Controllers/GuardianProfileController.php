<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\GuardianProfileRequest;
use App\GuardianProfile;

class GuardianProfileController extends Controller
{
    public function createGuardian(GuardianProfileRequest $request){
    	$input = $request->validated();
	    $user = GuardianProfile::create($input);
	    if($user){
	    	return redirect()->route('student.view', $input['student_id'])->with('success', 'Guardian created successfully!');
	    }
	    else{
	    	return redirect()->route('student.view', $input['student_id'])->with('fail', 'Guardian creation failed!');
	    } 
    }

    public function updateGuardian(GuardianProfileRequest $request){

    }
}
