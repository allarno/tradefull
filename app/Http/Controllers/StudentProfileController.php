<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StudentProfileRequest;
use App\StudentProfile;
use Carbon\Carbon;

class StudentProfileController extends Controller
{
	public function createStudent(StudentProfileRequest $request){
		$input = $request->validated();
		$input['date_of_birth'] = Carbon::createFromFormat('m/d/Y', $request->date_of_birth)->format('Y-m-d');
    $user = StudentProfile::create($input);  
    if($user){
    	return redirect()->route('student.new')->with('success', 'Student created successfully!');
    }
    else{
    	return redirect()->route('student.new')->with('fail', 'Student creation failed!');
    }        
	}

  public function getStudentGuardians($id){

  }

  public function getStudent($id){
  	$student = StudentProfile::get($id)->first();
  	return $student;
  }

  public function updateStudent(StudentProfileRequest $request, StudentProfile $studentProfile){
  	$input = $request->except(['_token', '_method', 'admission_number']);
    $input['date_of_birth'] = Carbon::createFromFormat('m/d/Y', $request->date_of_birth)->format('Y-m-d');
    StudentProfile::where('admission_number', $request->admission_number)->update($input);
   	return redirect()->route('students')->with('success', 'Student updated successfully!');
  }

  public function deleteStudent(Request $request){
  	$student = StudentProfile::where('id', $request->id)->firstOrFail()->delete();
  	return redirect()->route('students')->with('success', 'Student data deleted successfully');
  }
}
