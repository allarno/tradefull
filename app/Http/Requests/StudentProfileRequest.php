<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StudentProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->isMethod('POST')){
            return $this->createRules();
        }
        elseif($this->isMethod('PUT')){
            return $this->updateRules(); 
        }
    }

    public function createRules(){
        return [
                'first_name' => 'required',
                'last_name' => 'required',
                'other_name' => 'nullable',
                'admission_number' => 'required|alphaNum|unique:student_profiles,admission_number',
                'date_of_birth' => 'required|date',
                'sex' => 'required',
                'active' => 'required'
            ];
    }

    public function updateRules(){
        return [
                'first_name' => 'required',
                'last_name' => 'required',
                'other_name' => 'nullable',
                'admission_number' => 'required|alphaNum',
                'date_of_birth' => 'required|date',
                'sex' => 'required',
                'active' => 'required'
            ];
    }

    public function messages(){
        return [
            'admission_number.required' => ':Attribute is required',
            'first_name.required' => ':Attribute is required',
            'last_name.required' => ':Attribute is required',
            'date_of_birth.required' => ':Attribute is required',
            'date_of_birth.date' => ':Attribute should be in Y-m-d format',
            'sex.required' => ':Attribute is required',
            'active.required' => ':Attribute is required'
        ];
    }
}
