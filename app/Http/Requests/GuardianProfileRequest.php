<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GuardianProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->isMethod('POST')){
            return $this->createRules();
        }
        elseif($this->isMethod('PUT')){
            return $this->updateRules(); 
        }
    }

    public function createRules(){
        return [
            'student_id' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'other_name' => 'nullable',
            'relationship' => 'required',
            'sex' => 'required',
            'active' => 'required'
            ];
    }

    public function updateRules(){
        return [
            'student_id' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'other_name' => 'nullable',
            'relationship' => 'required',
            'sex' => 'required',
            'active' => 'required'
            ];
    }

    public function messages(){
        return [
            'student_id.required' => ':Attribute is required',
            'first_name.required' => ':Attribute is required',
            'last_name.required' => ':Attribute is required',
            'relationship.required' => ':Attribute is required',
            'sex.required' => ':Attribute is required',
            'active.required' => ':Attribute is required'
        ];
    }
}
