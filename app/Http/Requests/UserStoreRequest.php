<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|unique:user',
            'password' => 'required|alphaNum|8',
            'first_name' => 'required',
            'last_name' => 'required',
            'date_of_birth' => 'required'
            ];
    }

    public function messages(){
        return [
            'email.required' => ':Attribute is required',
            'password.required' => ':Attribute is required',
            'first_name' => ':Attribute is required',
            'last_name' => ':Attribute is required',
            'date_of_birth' => ':Attribute is required'
        ];
    }
}
