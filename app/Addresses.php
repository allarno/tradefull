<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Addresses extends Model
{
    use SoftDeletes;
    protected $table = 'addresses';
    protected $fillable = ['guardian_id', 'street', 'apartment', 'city', 'state', 'zip'];
}
