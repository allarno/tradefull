<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class StudentProfile extends Model
{
    use SoftDeletes;    
    protected $table = 'student_profiles';
    protected $fillable = ['admission_number', 'first_name', 'last_name', 'other_name', 'sex', 'active', 'profile_photo', 'date_of_birth'];
    
	public static function getStudents(){
		$results = StudentProfile::paginate(10);
		return $results;
	}

	public static function getStudent($id){
		$student = StudentProfile::find($id);
        if(!empty($student )){
	        return $student;
        } 
	}

	public function guardians(){
		return $this->hasMany(GuardianProfile::class, 'student_id');
	}

	public static function getAdmissionNumber($admission_number){
		return StudentProfile::query()->where('admission_number', '=', $admission_number)->get()->first();
	}
}
