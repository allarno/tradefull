<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GuardianProfile extends Model
{
    use SoftDeletes; 
    protected $table = 'guardian_profiles';
    protected $fillable = ['student_id', 'first_name', 'last_name', 'relationship', 'sex', 'other_name', 'active'];

    public function student(){
    	return $this->belongsTo(StudentProfile::class, 'student_id');
    }
}
