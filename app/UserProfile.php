<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class UserProfile extends Model
{
    public static function sex(){
		$sexes = ['Female', 'Male', 'Other'];
		return $sexes;
	}

	public static function getDateOfBirthAttribute($value)
	{
	    return Carbon::parse($value)->format('m/d/Y');
	}
}
